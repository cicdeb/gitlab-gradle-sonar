package com.example.demo.error;

import java.util.HashMap;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * The type Demo exception handler.
 */
@ControllerAdvice
@EnableWebMvc
public class DemoExceptionHandler {

   
    
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ResponseEntity<Object> handleNoHandlerFound(Exception e, WebRequest request) {
		HashMap<String, String> body = new HashMap<>();
		body.put("message", "invalid endpoint");
		return new ResponseEntity<>(body, HttpStatus.METHOD_NOT_ALLOWED);
	} 

}
